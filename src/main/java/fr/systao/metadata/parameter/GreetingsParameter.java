package fr.systao.metadata.parameter;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GreetingsParameter {
	@NotBlank
	private String name;
	
	private int hugs;

	private Date validity;
	
	private Style level;	
}
