package fr.systao.metadata.parameter;

public enum Style {
	ACADEMIC,
	FORMAL,
	COLLOQUIAL,
	STREET_LINGO;
}
