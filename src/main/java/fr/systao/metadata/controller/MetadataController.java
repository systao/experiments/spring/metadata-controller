package fr.systao.metadata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.systao.metadata.model.Interaction;
import fr.systao.metadata.model.Parameter;
import fr.systao.metadata.model.Target;

@RestController
@RequestMapping(path="/metadata/services")
public class MetadataController {
	
	@GetMapping
	public List<String> getServices() {
		return Arrays.asList("greet");
	}

	@GetMapping(path="/{service}")
	public Interaction getInteractions(@PathVariable String service) {
		return loadInteraction(service);
	}
	
	@GetMapping(path="/{service}/targets")
	public List<String> consolidateTargets(@PathVariable String service) {
		List<Target> targets = loadInteraction(service).getTargets();
		return targets.stream()
					  .map(Target::getName)
					  .collect(Collectors.toList());
	}
	
	@GetMapping(path="/{service}/targets/{target}")
	public Target consolidateTargets(@PathVariable String service, @PathVariable String target) {
		List<Target> targets = loadInteraction(service).getTargets();
		return targets.stream()
					  .filter(t -> StringUtils.equals(target, t.getName()))
					  .findFirst()
					  .get();
	}
	
	@GetMapping(path="/{service}/targets/consolidated")
	public Interaction consolidateTargets(
			@PathVariable String service, 
			@RequestParam(name="targets", required=false) List<String> targets) {
		
		Interaction source = loadInteraction(service);
		
		List<Target> selectedTargets = source.getTargets()
				   .stream()
				   .filter(t -> t.match(targets))
				   .collect(Collectors.toList());
		
		Interaction interaction = Interaction.of(source);
		
		
		@SuppressWarnings("unused")
		Map<String, List<Parameter>> parameters = selectedTargets.stream()
				.flatMap(t -> t.getParameters().stream())
				.collect(Collectors.groupingBy(p -> p.getName()));
		
//		Target target = new Target();
//		target.setName("consolidated").setParameters(null);
		
		return interaction;
	}

	private Interaction loadInteraction(String service) {
		Properties map = getConfig(service);
		
		ConfigurationPropertySource source = new MapConfigurationPropertySource(map);
        
        Binder binder = new Binder(source);
        
        return binder.bind("", Interaction.class).get();
	}

	private Properties getConfig(String greet) {
		Resource resource = new ClassPathResource(String.join("", "/interactions/", greet, ".yml"));
		YamlPropertiesFactoryBean factoryBean = new YamlPropertiesFactoryBean();
		factoryBean.setResources(resource);
		return factoryBean.getObject();
	}
}
