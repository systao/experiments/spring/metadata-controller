package fr.systao.metadata.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.systao.metadata.parameter.GreetingsParameter;
import fr.systao.metadata.service.GreetingsService;

@RestController
@RequestMapping(path="/greetings")
public class GreetingsController {
	@Autowired
	private List<GreetingsService> services;
	
	@PostMapping
	public List<String> greet(GreetingsParameter parameter) {
		return services
			.parallelStream()
			.map(s -> s.greet(parameter))
			.collect(Collectors.toList());
	}
	
}
