package fr.systao.metadata.service;

import fr.systao.metadata.parameter.GreetingsParameter;

public interface GreetingsService {
	String greet(GreetingsParameter paramter);
}
