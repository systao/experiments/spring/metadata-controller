package fr.systao.metadata.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class Multiplicity {
	private Bound lowerBound;
	
	private Bound upperBound;
	
	@Getter
	@Setter
	public static final class Bound {
		private int value;
		private boolean inclusive = true;
	}
}