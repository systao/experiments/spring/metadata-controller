package fr.systao.metadata.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter 
@Getter
@Accessors(chain=true)
@JsonInclude(Include.NON_EMPTY)
public class Target {
	private static final String ANY = "any";

	private String name;
	
	private List<Parameter> parameters;

	private Access access;
	
	private String example;
	
	public boolean match(List<String> targets) {
		return StringUtils.equals(name, ANY) || targets.contains(name);
	}
}
