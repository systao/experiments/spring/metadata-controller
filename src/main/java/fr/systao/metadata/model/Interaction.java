package fr.systao.metadata.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain=true)
@JsonInclude(Include.NON_EMPTY)
public class Interaction {
	private String name;
	
	private List<Target> targets;
	
	private String method;
	
	private Access access;

	public static Interaction of(Interaction source) {
		return new Interaction()
			.setAccess(source.getAccess())
			.setMethod(source.getMethod())
			.setName(source.getName());
	}
}
