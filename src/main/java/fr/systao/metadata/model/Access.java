package fr.systao.metadata.model;

public enum Access {
	authenticated,
	anonymous;
}
