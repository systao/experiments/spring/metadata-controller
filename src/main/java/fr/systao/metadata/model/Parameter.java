package fr.systao.metadata.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class Parameter {
	private String name;

	private Multiplicity multiplicity;

	private List<Property> properties;
	
	private List<Constraint> constraints;
}