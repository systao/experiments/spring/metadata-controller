package fr.systao.metadata.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(Include.NON_EMPTY)
public class Property {
	private String name;
	
	private String description;
	
	private String type;
	
	private boolean required = true;
	
	private List<Constraint> constraints;
	
	private List<String> values;
	
	private String defaultValue;
}
